#!/bin/bash
sudo apt update
sudo apt upgrade -y
sudo apt install -y gcc g++ cmake make valgrind libjson-c-dev libapr1-dev
# For Debian only ######
ln -s /usr/include/apr-1.0 /usr/include/apr-1
########################
mkdir build
cd build
cmake ../
make
chmod +x test1
./test1
