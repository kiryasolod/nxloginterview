#include <apr-1/apr_file_io.h>
#include <apr-1/apr_pools.h>
#include <apr-1/apr_strings.h>
#include <stdio.h>

#include "../io/io.h"
#include "../parser/parser.h"
#define LINE_LENGTH 2048

const char *TEST_JSON =
    "{ \"key1\": \"value\", \"key2\": 42, \"key3\": true, \"keygdhdgd4\": false, \"bla\": \"bbbb\" "
    "}\n{ \"sadsf\": \"dsewtew\", \"dsre\": 3221, \"sdfds\": \"dsfewew\" }\n";

apr_status_t compare_text_files(apr_pool_t *pool, const char *file1, const char *file2) {
  apr_file_t *fd1;
  apr_file_t *fd2;

  apr_finfo_t info1;
  apr_finfo_t info2;

  apr_status_t result = open_text_file_r(pool, &fd1, file1);
  if (result != APR_SUCCESS) return result;
  result = open_text_file_r(pool, &fd2, file2);
  if (result != APR_SUCCESS) return result;

  result = apr_file_info_get(&info1, APR_FINFO_SIZE, fd1);
  if (result != APR_SUCCESS) return result;
  result = apr_file_info_get(&info2, APR_FINFO_SIZE, fd2);
  if (result != APR_SUCCESS) return result;

  if (info1.size != info2.size) {
    result = 1;
    goto closing;
  };

  while (1) {
    char c1;
    char c2;
    size_t size = sizeof(char);
    if (apr_file_read(fd1, &c1, &size) == APR_EOF) break;
    if (apr_file_read(fd2, &c2, &size) == APR_EOF) break;
    if (c1 != c2) {
      result = 1;
      goto closing;
    }
  }

closing:
  apr_file_close(fd1);
  apr_file_close(fd2);
  return result;
}

int main(int argc, char **argv) {
  apr_app_initialize(&argc, (const char *const **)argv, NULL);
  size_t TEST_JSON_SIZE = strlen(TEST_JSON);
  apr_pool_t *main_pool;
  apr_pool_create(&main_pool, NULL);
  apr_file_t *json_source;
  apr_status_t result = open_text_file_w(main_pool, &json_source, "source.json");

  if (result != APR_SUCCESS) return result;
  result = apr_file_write(json_source, TEST_JSON, &TEST_JSON_SIZE);
  if (result != APR_SUCCESS) return result;
  result = apr_file_close(json_source);
  if (result != APR_SUCCESS) return result;

  result = dump_json_log(main_pool, "source.json", "dump.bin", "keystore.bin", LINE_LENGTH);
  if (result != APR_SUCCESS) {
    fprintf(stderr, "Something got wrong during a dump process");
    apr_terminate();
    return result;
  }

  result =
      restore_json_from_dump(main_pool, "dump.bin", "keystore.bin", "restored.json", LINE_LENGTH);
  if (result != APR_SUCCESS) {
    fprintf(stderr, "Something got wrong during a restore process");
    apr_terminate();
    return result;
  }

  result = compare_text_files(main_pool, "source.json", "restored.json");
  apr_terminate();
  return result;
}
