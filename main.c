#include <apr-1/apr_file_io.h>
#include <apr-1/apr_pools.h>
#include <apr-1/apr_strings.h>
#include <stdio.h>

#include "parser/parser.h"
#define LINE_LENGTH 2048

/* Defining an enum of argv positions to make the code more readable
   and to avoid mistakes with argv indexes */
typedef enum { Source = 1, Dump = 2, Keystore = 3, Argc = 4 } file_args;

int main(int argc, char **argv) {
  /* Checking the arguments */
  if (argc < Argc) {
    printf(
        "Using: NXLogInterview </path/to/source.json> </path/to/dump.bin> "
        "</path/to/keystore.bin\n");
    return 1;
  }
  /* Initializing APR and create a main memory pool */
  apr_app_initialize(&argc, (const char *const **)argv, NULL);
  apr_pool_t *main_pool;
  apr_pool_create(&main_pool, NULL);
  apr_status_t status =
      dump_json_log(main_pool, argv[Source], argv[Dump], argv[Keystore], LINE_LENGTH);
  if (status != APR_SUCCESS) { fprintf(stderr, "Something got wrong during a dump process"); }
  status =
      restore_json_from_dump(main_pool, argv[Dump], argv[Keystore], "restored.json", LINE_LENGTH);
  if (status != APR_SUCCESS) { fprintf(stderr, "Something got wrong during a restore process"); }
  apr_pool_destroy(main_pool);
  apr_terminate();
  return status;
}
