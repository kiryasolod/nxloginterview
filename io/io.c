#include "io.h"

#include "../parser/utils.h"

apr_status_t open_text_file_r(apr_pool_t *pool, apr_file_t **fd, const char *file_name) {
  apr_status_t status = APR_SUCCESS;
  status              = apr_file_open(fd, file_name, APR_FOPEN_READ, APR_OS_DEFAULT, pool);
  if (status != APR_SUCCESS) {
    fprintf(stderr, "Failed to open a file for reading in text mode %s\n", file_name);
  }
  return status;
}

apr_status_t open_text_file_w(apr_pool_t *pool, apr_file_t **fd, const char *file_name) {
  apr_status_t status = APR_SUCCESS;
  status              = apr_file_open(
      fd, file_name, APR_FOPEN_WRITE | APR_FOPEN_CREATE | APR_FOPEN_TRUNCATE, APR_OS_DEFAULT, pool);
  if (status != APR_SUCCESS) {
    fprintf(stderr, "Failed to open a file for writing in binary mode %s\n", file_name);
  }
  return status;
}

apr_status_t open_bin_file_r(apr_pool_t *pool, apr_file_t **fd, const char *file_name) {
  apr_status_t status = APR_SUCCESS;
  status = apr_file_open(fd, file_name, APR_FOPEN_READ | APR_FOPEN_BINARY, APR_OS_DEFAULT, pool);
  if (status != APR_SUCCESS) {
    fprintf(stderr, "Failed to open a file for reading in binary mode %s\n", file_name);
  }
  return status;
}

apr_status_t open_bin_file_w(apr_pool_t *pool, apr_file_t **fd, const char *file_name) {
  apr_status_t status = APR_SUCCESS;
  // clang-format off
  status = apr_file_open(fd, file_name, APR_FOPEN_WRITE | APR_FOPEN_CREATE
    | APR_FOPEN_BINARY | APR_FOPEN_TRUNCATE, APR_OS_DEFAULT, pool);
  // clang-format on
  if (status != APR_SUCCESS) {
    fprintf(stderr, "Failed to open a file for writing in binary mode %s\n", file_name);
  }
  return status;
}

uint8_t read_tlv_object(apr_pool_t *tlv_pool, apr_file_t *fd, tlv_object *result) {
  /* Checking if it is the end of the file */
  apr_status_t status = apr_file_eof(fd);
  if (status != APR_SUCCESS) return 0; /* Returning false. The end of file */
  /* Reading how many key-value pairs are in the object */
  size_t length = sizeof(tlv_pairs_count_t);
  status        = apr_file_read(fd, (void *)&result->pairs_count, &length);
  if (status != APR_SUCCESS) return 0; /* Returning false. The end of file */
  /* Parsing all the pairs of the object before a delimiter */
  while (apr_file_eof(fd) != APR_EOF && result->pairs_count--) {
    tlv_pair *pair = apr_palloc(tlv_pool, sizeof(tlv_pair));
    memset(pair, 0, sizeof(tlv_pair));
    length = sizeof(pair->key);
    status = apr_file_read(fd, (void *)&pair->key, &length);
    if (status != APR_SUCCESS) return 0;
    length = sizeof(pair->type);
    status = apr_file_read(fd, (void *)&pair->type, &length);
    if (status != APR_SUCCESS) return 0;
    if (pair->type == String) {
      length = sizeof(pair->length);
      status = apr_file_read(fd, (void *)&pair->length, &length);
      if (status != APR_SUCCESS) return 0;
      length = pair->length;
      /* Incrementing a size to add ending zero*/
      ++(pair->length);
      pair->value = apr_palloc(tlv_pool, pair->length);
      /* Reading a value using old length before increment */
      status = apr_file_read(fd, pair->value, &length);
      if (status != APR_SUCCESS) return 0;
      /* Inserting zero to the last position */
      ((char *)pair->value)[length] = 0;
    } else {
      length      = get_length_of_type(pair->type);
      pair->value = apr_palloc(tlv_pool, length);
      status      = apr_file_read(fd, pair->value, &length);
      if (status != APR_SUCCESS) return 0;
    }
    /* Writing parsed pair to object */
    if (result->content == NULL) {
      result->content = pair;
    } else {
      /* Here the function cannot return NULL, so the segfault is impossible */
      get_last_pair_of_object(result)->next = pair;
    }
  };
  /* Object has been read successfully. Returning true */
  return 1;
}

apr_status_t write_tlv_object(tlv_object *obj, apr_file_t *fd) {
  apr_status_t result = APR_SUCCESS;
  size_t field_size   = sizeof(tlv_pairs_count_t);
  /* Writing a count of tlv_pairs */
  result = apr_file_write(fd, (const void *)&obj->pairs_count, &field_size);
  if (result != APR_SUCCESS) {
    printf("Failed write a tlv object into dump file\n");
    return result;
  }
  /* Writing all values in TLV form*/
  tlv_pair *pair = obj->content;
  while (pair != NULL) {
    field_size = sizeof(pair->key);
    result     = apr_file_write(fd, (const void *)&pair->key, &field_size);
    if (result != APR_SUCCESS) { break; }
    field_size = sizeof(pair->type);
    result     = apr_file_write(fd, (const void *)&pair->type, &field_size);
    if (result != APR_SUCCESS) { break; }
    if (pair->type == String) {
      field_size = sizeof(pair->length);
      result     = apr_file_write(fd, (const void *)&pair->length, &field_size);
      if (result != APR_SUCCESS) { break; }
    }
    field_size = pair->length;
    result     = apr_file_write(fd, pair->value, &field_size);
    if (result != APR_SUCCESS) { break; }
    pair = pair->next;
  }
  return result;
}

apr_status_t write_keystore(apr_pool_t *keystore_pool, apr_hash_t *keystore, apr_file_t *fd) {
  apr_hash_index_t *index;
  apr_status_t result = APR_SUCCESS;
  // Iterate over each element in the hash table
  for (index = apr_hash_first(keystore_pool, keystore); index; index = apr_hash_next(index)) {
    const char *key;
    uint8_t *data;
    apr_size_t field_size         = 0;
    apr_size_t size_of_field_size = sizeof(keystore_key_length_t);
    apr_hash_this(index, (const void **)&key, (apr_ssize_t *)&field_size, (void **)&data);
    // ---------------------------------------------------------------------------
    // | key-size - 2 bytes | key - key-size value of bytes | value - 2 bytes |...
    // ---------------------------------------------------------------------------
    result = apr_file_write(fd, &field_size, &size_of_field_size);  // key-size
    if (result != APR_SUCCESS) { break; }
    result = apr_file_write(fd, key, &field_size);  // key itself
    if (result != APR_SUCCESS) { break; }
    field_size = sizeof(keystore_key_value_t);           // the size of a value
    result     = apr_file_write(fd, data, &field_size);  // value itself
  }
  if (result != APR_SUCCESS) { fprintf(stderr, "Failed to write keystore hash-table to file"); }
  return result;
}

apr_status_t read_keystore(apr_file_t *fd, apr_pool_t *keystore_pool, apr_hash_t *keystore) {
  apr_status_t result = apr_file_eof(fd);
  if (result == APR_EOF) return result;
  /* Reading all the records */
  do {
    keystore_key_length_t key_length = 0;

    /* Reading a length of a key... */
    apr_size_t length = sizeof(key_length);
    result            = apr_file_read(fd, (void *)&key_length, &length);
    if (result != APR_SUCCESS) return result;

    /* Reading a key itself */
    length = key_length;
    /* Allocating by length including ending zero */
    char *key       = apr_palloc(keystore_pool, key_length + 1);
    key[key_length] = 0;

    result = apr_file_read(fd, key, &length);
    if (result != APR_SUCCESS) return result;

    /* Rading a value... */
    length                      = sizeof(keystore_key_value_t);
    keystore_key_value_t *value = apr_palloc(keystore_pool, sizeof(keystore_key_value_t));
    result                      = apr_file_read(fd, (void *)value, &length);
    if (result != APR_SUCCESS) return result;

    /* Everything is been read. Now creating a new keystore record */
    apr_hash_set(keystore, key, APR_HASH_KEY_STRING, value);
  } while (apr_file_eof(fd) != APR_EOF);
  return result;
}
