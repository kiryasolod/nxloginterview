#pragma once
#include <apr-1/apr_file_io.h>
#include <apr-1/apr_pools.h>
#include <apr-1/apr_strings.h>

#include "../parser/types.h"

/// @brief Open a file in text format for reading
/// @param pool a memory pool. Must be initialized
/// @param fd a file descriptor
/// @param file_name a full name of file including it's path
/// @return a status of opening. APR_SUCCESS if everything is ok
apr_status_t open_text_file_r(apr_pool_t *pool, apr_file_t **fd, const char *file_name);

/// @brief Open a file in text format for writing
/// @param pool a memory pool. Must be initialized
/// @param fd a file descriptor
/// @param file_name a full name of file including it's path
/// @return a status of opening. APR_SUCCESS if everything is ok
apr_status_t open_text_file_w(apr_pool_t *pool, apr_file_t **fd, const char *file_name);

/// @brief Open a file in binary format for reading
/// @param pool a memory pool. Must be initialized
/// @param fd a file descriptor
/// @param file_name a full name of file including it's path
/// @return a status of opening. APR_SUCCESS if everything is ok
apr_status_t open_bin_file_r(apr_pool_t *pool, apr_file_t **fd, const char *file_name);

/// @brief Open a file in binary format for writing
/// @param pool a memory pool. Must be initialized
/// @param fd a file descriptor
/// @param file_name a full name of file including it's path
/// @return a status of opening. APR_SUCCESS if everything is ok
apr_status_t open_bin_file_w(apr_pool_t *pool, apr_file_t **fd, const char *file_name);

/// @brief Reads tlv_object from opened file
/// @param tlv_pool a memory pool that will store the tlv_object
/// @param fd a file descriptor. Must be initialized. File must be already
/// opened
/// @param result. returned tlv_object itself that was written from the file. Must be initialized
/// @return a boolean value that signs if the object was read and it's not the
/// end of file
uint8_t read_tlv_object(apr_pool_t *tlv_pool, apr_file_t *fd, tlv_object *result);

/// @brief Writes a whole tlv object to a file
/// @param obj - a tlv object. Must be fully initialized and ready to be written
/// @param fd  - a destination file descriptor. Must be initialized, a file must
/// be opened
/// @return result of writing
apr_status_t write_tlv_object(tlv_object *obj, apr_file_t *fd);

/// @brief Writes a keystore to a file
/// A keystore is a map of string to uint16_t which is written to a file using the following TLV endoding format:
/// ---------------------------------------------------------------------------
/// | key-size - 2 bytes | key - key-size value of bytes | value - 2 bytes |...
/// ---------------------------------------------------------------------------
/// @param keystore_pool a keystore pool. Must be initialized
/// @param keystore a keystore itself. Must be initialized
/// @param fd a destinanion file descriptor. Must be initialized, a file must be
/// opened
/// @return result of writing
apr_status_t write_keystore(apr_pool_t *keystore_pool, apr_hash_t *keystore, apr_file_t *fd);

apr_status_t read_keystore(apr_file_t *fd, apr_pool_t *keystore_pool, apr_hash_t *keystore);
