#pragma once
#include <apr-1/apr_file_io.h>
#include <apr-1/apr_hash.h>
#include <apr-1/apr_pools.h>
#include <json-c/json.h>
#include <math.h>
#include <memory.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/// All types for TLV encoding. Using only one byte for them
/// @warning The values of this enum intentionally start from 1, not from 0!
typedef enum { Boolean = 1, String = 2, Llong = 3, Double = 4 } tlv_types;

typedef uint16_t tlv_key_t;
typedef uint8_t tlv_type_t;
typedef uint8_t tlv_pairs_count_t;
typedef uint32_t tlv_length_t;

typedef uint16_t keystore_key_length_t;
typedef uint16_t keystore_key_value_t;

/// @brief A pair of key, represented as integer and must be associated with an original string in JSON,
/// and value that can be any of a primitive types. The type field must accept an enum tlv_types.
/// Length is a size of the value buffer
typedef struct tlv_pair {
  tlv_key_t key;
  tlv_type_t type;
  tlv_length_t length;
  void *value;
  struct tlv_pair *next;
} tlv_pair;

/// @brief A simple dynamic array of tlv_pair pointers with it's length
typedef struct tlv_object {
  tlv_pairs_count_t pairs_count;
  tlv_pair *content;
} tlv_object;
