#include "parser.h"

#include "../io/io.h"

apr_status_t dump_json_log(apr_pool_t *pool, const char *source_file, const char *dest_file,
    const char *keystore_file, const size_t max_line_length) {
  apr_status_t result = APR_SUCCESS;
  /* Creating an internal pool to keep everything inside */
  apr_pool_t *internal_pool;
  apr_pool_create(&internal_pool, pool);
  /* All derived pools will be children of this internal pool. No need to call their cleaning */
  /* Creating pool and descriptor for source file */
  apr_pool_t *source_pool;
  apr_pool_create(&source_pool, internal_pool);
  apr_file_t *source_fd;

  /* Opening a source file */
  result = open_text_file_r(source_pool, &source_fd, source_file);
  if (result != APR_SUCCESS) {
    apr_pool_destroy(source_pool);
    return result;
  }

  /* Creating pool for keystore file */
  apr_pool_t *keystore_pool;
  apr_pool_create(&keystore_pool, internal_pool);
  /* Creating a hash table*/
  apr_hash_t *keystore = apr_hash_make(keystore_pool);

  /* Creating pool and descriptor for dump file */
  apr_pool_t *dump_pool;
  apr_pool_create(&dump_pool, internal_pool);
  apr_file_t *dump_fd;

  /* Opening a dump file */
  result = open_bin_file_w(dump_pool, &dump_fd, dest_file);
  if (result != APR_SUCCESS) {
    apr_pool_destroy(internal_pool);
    apr_file_close(source_fd);
    return result;
  }

  /* Converting json to tlv line by line and writing to dump */
  apr_status_t read_result;
  char *line = apr_palloc(pool, max_line_length);
  while ((read_result = apr_file_gets(line, max_line_length, source_fd)) == APR_SUCCESS) {
    apr_pool_t *tlv_pool;
    /* Initialize a memory pool for a binary tlv records */
    apr_pool_create(&tlv_pool, internal_pool);
    tlv_object *object = json_object_to_tlv_object(tlv_pool, keystore_pool, line, keystore);
    /* Writing a tlv object to file */
    result = write_tlv_object(object, dump_fd);
    /* Destroy the tlv memory pool */
    apr_pool_destroy(tlv_pool);
    if (result != APR_SUCCESS) {
      apr_file_close(source_fd);
      apr_file_close(dump_fd);
      apr_pool_destroy(internal_pool);
      return result;
    }
  }

  /* Checking that all source file is been read */
  if (read_result != APR_EOF) {
    apr_file_close(source_fd);
    apr_file_close(dump_fd);
    apr_pool_destroy(internal_pool);
    return result;
  }

  /* Opening a keystore file */
  apr_file_t *keystore_fd;
  result = open_bin_file_w(keystore_pool, &keystore_fd, keystore_file);
  if (result != APR_SUCCESS) {
    apr_pool_destroy(internal_pool);
    apr_file_close(source_fd);
    apr_file_close(dump_fd);
    return result;
  }

  /* Writing a hash-table */
  result = write_keystore(keystore_pool, keystore, keystore_fd);

  apr_file_close(source_fd);
  apr_file_close(dump_fd);
  apr_file_close(keystore_fd);
  apr_pool_destroy(internal_pool);
  return result;
}

apr_status_t restore_json_from_dump(apr_pool_t *pool, const char *dump_file,
    const char *keystore_file, const char *dest_json_file, const size_t max_line_length) {
  apr_status_t result = APR_SUCCESS;
  /* Creating an internal pool to keep everything inside */
  apr_pool_t *internal_pool;
  apr_pool_create(&internal_pool, pool);
  /* All derived pools will be children of this internal pool. No need to call their cleaning */
  /* Creating pool and descriptor for dump file */
  apr_pool_t *dump_pool;
  apr_pool_create(&dump_pool, internal_pool);
  apr_file_t *dump_fd;

  /* Opening a dump file */
  result = open_bin_file_r(dump_pool, &dump_fd, dump_file);
  if (result != APR_SUCCESS) {
    apr_pool_destroy(internal_pool);
    return result;
  }

  /* Creating pool for keystore file */
  apr_pool_t *keystore_pool;
  apr_pool_create(&keystore_pool, internal_pool);
  /* Creating a hash table*/
  apr_hash_t *keystore = apr_hash_make(keystore_pool);
  apr_file_t *keystore_fd;

  /* Opening a keystore file */
  result = open_bin_file_r(keystore_pool, &keystore_fd, keystore_file);
  if (result != APR_SUCCESS) {
    apr_pool_destroy(internal_pool);
    return result;
  }

  /* Reading a keystore from a file */
  result = read_keystore(keystore_fd, keystore_pool, keystore);
  /* If there is an error and the file wasn't read to the end, terminating */
  if (result != APR_EOF) {
    fprintf(stderr, "Something went wrong while reading a keystore file");
    exit(1);
  }

  /* Closing a keystore file. We don't need it anymore */
  result = apr_file_close(keystore_fd);
  if (result != APR_SUCCESS) {
    fprintf(stderr, "Cannot close a keystore file");
    exit(1);
  }
  /* Creating pool and descriptor for dest file */
  apr_pool_t *dest_pool;
  apr_pool_create(&dest_pool, internal_pool);
  apr_file_t *dest_fd;

  /* Opening a dest file */
  result = open_text_file_w(dest_pool, &dest_fd, dest_json_file);
  if (result != APR_SUCCESS) {
    apr_file_close(dump_fd);
    apr_pool_destroy(internal_pool);
    return result;
  }

  /* Now restoring data */
  while (1) {
    apr_pool_t *tlv_pool;
    apr_pool_create(&tlv_pool, internal_pool);
    tlv_object *object = apr_palloc(tlv_pool, sizeof(tlv_object));
    memset(object, 0, sizeof(tlv_object));
    if (!read_tlv_object(tlv_pool, dump_fd, object)) {
      apr_pool_destroy(tlv_pool);
      break;
    }
    json_object *json    = tlv_object_to_json_object(keystore_pool, object, keystore);
    const char *json_str = json_object_to_json_string(json);
    size_t json_str_size = strlen(json_str);
    const char *newline  = "\n";
    size_t newline_size  = strlen(newline);
    result               = apr_file_write(dest_fd, json_str, &json_str_size);
    if (result != APR_SUCCESS) {
      json_object_put(json);
      apr_pool_destroy(tlv_pool);
      break;
    }
    result = apr_file_write(dest_fd, newline, &newline_size);
    json_object_put(json);
    apr_pool_destroy(tlv_pool);
    if (result != APR_SUCCESS) { break; }
  }
  apr_file_close(dump_fd);
  apr_file_close(dest_fd);
  apr_pool_destroy(internal_pool);
  return result;
}
