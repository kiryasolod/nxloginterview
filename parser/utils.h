#pragma once
#include "types.h"
/// @brief A macro for defining the maximum value of unsigned type
#define UINT_TYPE_MAX_VALUE(type) (uint64_t) powf(2, sizeof(type) * 8) - 1

/// @brief Returns the first key that has the given value in the hash table
const char *get_key_by_value(apr_hash_t *hash, const void *value, apr_ssize_t value_size);

/// @brief Gets the last tlv_pair of the tlv_object
/// @param object An object to search in. Must be initialized!
/// @return A pointer to the last pair or NULL if the object is empty
tlv_pair *get_last_pair_of_object(tlv_object *object);

/// @brief Gets a zise of the type. Returns 0 if the type is String
/// @param type An enum of type
/// @return Length of the type in bytes
tlv_length_t get_length_of_type(tlv_types type);

/// @brief Gets a maximum value in a keystore (hash-table of JSON keys)
/// @param keystore_pool A memory pool that stores a keystore
/// @param keystore A hash-talbe itself
/// @return Maximum value in keystore
uint16_t get_max_keystore_value(apr_pool_t *keystore_pool, apr_hash_t *keystore);

/// @brief Generates a TLV-record based on string key and json_object value
/// @param tlv_pool A memory pool that stores a tlv_pair that is returned
/// @param keystore_pool A memory pool for a hash-table of JSON keys
/// @param key A string-representation of a JSON key
/// @param value A value that is represented as json_object
/// @param keystore A hash-table itself. Stores JSON keys with their uint16_t
/// associations
/// @return Generated tlv_pair. A key-value structure that is ready to be
/// written
tlv_pair *generate_tlv_pair(apr_pool_t *tlv_pool, apr_pool_t *keystore_pool, const char *key,
    struct json_object *value, apr_hash_t *keystore);

/// @brief Converts a JSON string to a binary record of TLV encoding
/// A JSON string like {"key1": "value1", "key2": true, "key3" : 34 ...} converts
/// to a binary record of TLV encoding with the following format:
/// ------------------------------------------------------------------------
/// | key - 2 bytes | type - 1 byte | length - 4 bytes | value - n bytes |...
/// ------------------------------------------------------------------------
/// The hash table must be provided to store keys, so if there is an empty hash table,
/// the result of the function will be:
/// | 1 | 2 | 6 | value1 | 2 | 1 | 1 | 3 | 3 | 34 |
///   |                    |           |>{key: 3, type: 3(Llong), value: 34} number. Strictly 8 bytes, implicit
///   |                    |>{key: 2, type: 1(Boolean), value: 1}> bool. Strictly 1 byte, implicit
///  |>{key: 1, type: 2(String), length: 6 value: "value1"}> string. Dynamic type, so the length is included
/// The hash table will contain {"key1": 1, "key2": 2, "key3": 3}
/// @warning All arguments must be initialized before this funcion is called!
/// @param tlv_pool a pool for a binary object which is returned by the function
/// @param keystore_pool a pool for a hash-table of the keys
/// @param json_str incoming string with a json-like content. Must not have nested objects!
/// @param keystore a hash-table for storing keys to be represented in uint16_t as integers
/// @return a tlv object. A binary representation of the JSON object with keys that represented as integers instead of strings
tlv_object *json_object_to_tlv_object(
    apr_pool_t *tlv_pool, apr_pool_t *keystore_pool, const char *json_str, apr_hash_t *keystore);

json_object *tlv_object_to_json_object(
    apr_pool_t *keystore_pool, tlv_object *object, apr_hash_t *keystore);
