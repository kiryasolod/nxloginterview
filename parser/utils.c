#include "utils.h"

const char *get_key_by_value(apr_hash_t *hash, const void *value, apr_ssize_t value_size) {
  apr_hash_index_t *index;
  const char *key;
  void *data;

  // Iterate over each element in the hash table
  for (index = apr_hash_first(NULL, hash); index; index = apr_hash_next(index)) {
    apr_hash_this(index, (const void **)&key, NULL, &data);

    // Compare the value with the current element's value
    if (memcmp(value, data, value_size) == 0) {
      return key;  // Return the current element's key
    }
  }
  return NULL;  // Value not found in hash table
}

tlv_pair *get_last_pair_of_object(tlv_object *object) {
  tlv_pair *pair = object->content;
  if (pair == NULL) return NULL;
  while (pair->next != NULL) { pair = pair->next; }
  return pair;
}

tlv_length_t get_length_of_type(tlv_types type) {
  switch (type) {
    case Boolean: return sizeof(uint8_t);
    case Llong: return sizeof(uint64_t);
    case Double: return sizeof(double);
    default: return 0;
  }
}

uint16_t get_max_keystore_value(apr_pool_t *keystore_pool, apr_hash_t *keystore) {
  uint16_t max_keystore_value = 0;
  for (apr_hash_index_t *idx = apr_hash_first(keystore_pool, keystore); idx;
       idx                   = apr_hash_next(idx)) {
    void *value_ptr                  = apr_hash_this_val(idx);
    const keystore_key_value_t value = *(keystore_key_value_t *)value_ptr;
    if (value > max_keystore_value) { max_keystore_value = value; }
  }
  return max_keystore_value;
}

tlv_pair *generate_tlv_pair(apr_pool_t *tlv_pool, apr_pool_t *keystore_pool, const char *key,
    struct json_object *value, apr_hash_t *keystore) {
  tlv_pair *pair = apr_palloc(tlv_pool, sizeof(tlv_pair));
  /* Making a copy of the key */
  const size_t key_size = strlen(key);
  char *key_copy        = apr_palloc(keystore_pool, key_size);
  strncpy(key_copy, key, key_size);
  /* Setting all to zero */
  memset(pair, 0, sizeof(tlv_pair));
  /* Checking if we already have this key in a hash table */
  keystore_key_value_t *value_in_hash_ptr = apr_hash_get(keystore, key_copy, key_size);
  if (value_in_hash_ptr != NULL) { /* If there is already a key, taking it */
    pair->key = *(keystore_key_value_t *)value_in_hash_ptr;
  } else { /* If don't, creating one... */
    /* Allocating memory for a value */
    keystore_key_value_t *max_keystore_value =
        apr_palloc(keystore_pool, sizeof(keystore_key_value_t));
    /* Getting a max value among others that may already exist. Adding 1 */
    *max_keystore_value = get_max_keystore_value(keystore_pool, keystore) + 1;
    if (*max_keystore_value == UINT_TYPE_MAX_VALUE(keystore_key_value_t)) {
      fprintf(stderr, "Too many keys in the keystore! Program will be terminated!");
      exit(1);
    }
    /* Inserting a value to a hash table */
    apr_hash_set(keystore, key_copy, key_size, max_keystore_value);
    pair->key = *max_keystore_value;
  }
  /* Getting value, it's type and size from json */
  /* There are 4 possible types: string, integer, double and boolean */
  if (json_object_is_type(value, json_type_boolean)) {
    pair->type              = Boolean;
    pair->length            = get_length_of_type(pair->type);
    pair->value             = apr_palloc(tlv_pool, pair->length);
    *(uint8_t *)pair->value = json_object_get_boolean(value);
  } else if (json_object_is_type(value, json_type_double)) {
    pair->type             = Double;
    pair->length           = get_length_of_type(pair->type);
    pair->value            = apr_palloc(tlv_pool, pair->length);
    *(double *)pair->value = json_object_get_double(value);
  } else if (json_object_is_type(value, json_type_int)) {
    pair->type              = Llong;
    pair->length            = get_length_of_type(pair->type);
    pair->value             = apr_palloc(tlv_pool, pair->length);
    *(int64_t *)pair->value = json_object_get_int64(value);
  } else if (json_object_is_type(value, json_type_string)) {
    pair->type   = String;
    pair->length = json_object_get_string_len(value);
    pair->value  = apr_palloc(tlv_pool, pair->length);
    memmove(pair->value, json_object_get_string(value), pair->length);
  } else {
    fprintf(stderr, "Unsupported type stored in JSON! Terminating...");
    exit(1);
  }
  return pair;
}

tlv_object *json_object_to_tlv_object(
    apr_pool_t *tlv_pool, apr_pool_t *keystore_pool, const char *json_str, apr_hash_t *keystore) {
  /* Creating a json object */
  json_object *obj = json_tokener_parse(json_str);
  /* Allocating memory for a tlv_object */
  tlv_object *result = apr_palloc(tlv_pool, sizeof(tlv_object));
  /* Set all to zero */
  memset(result, 0, sizeof(tlv_object));
  /* Iterating KVPs inside the object */
  json_object_object_foreach(obj, key, value) {
    /* Creating a tlv pair */
    tlv_pair *current = generate_tlv_pair(tlv_pool, keystore_pool, key, value, keystore);
    ++(result->pairs_count);
    if (result->content == NULL) {
      /* Setting the content of a result only once */
      result->content = current;
    } else {
      /* Setting cursor to the first pair */
      tlv_pair *pair = result->content;
      /* Searching the last pair jumping the list */
      while (pair->next != NULL) { pair = pair->next; }
      /* Setting the next pair, so the last pair is not the last anymore */
      pair->next = current;
    }
  };
  /* Deleting json object */
  json_object_put(obj);
  return result;
}

json_object *tlv_object_to_json_object(
    apr_pool_t *keystore_pool, tlv_object *object, apr_hash_t *keystore) {
  json_object *json = json_object_new_object();
  tlv_pair *pair    = object->content;
  while (pair != NULL) {
    const char *key = get_key_by_value(keystore, (const void *)&pair->key, sizeof(pair->key));
    if (key == NULL) {
      fprintf(stderr,
          "Cannot find a key in hash table to build a json object! "
          "Terminating...");
      exit(1);
    }
    json_object *value;
    switch (pair->type) {
      case String: value = json_object_new_string(pair->value); break;
      case Boolean: value = json_object_new_boolean(*(uint8_t *)pair->value); break;
      case Llong: value = json_object_new_int64(*(int64_t *)pair->value); break;
      case Double: value = json_object_new_double(*(double *)pair->value); break;
      default: {
        fprintf(stderr, "Wrong type of json data! Terminating...");
        exit(1);
      } break;
    }
    json_object_object_add(json, key, value);
    pair = pair->next;
  }
  return json;
}
