#pragma once
#include <stdlib.h>

#include "utils.h"

/// @brief Reads JSON source file line by line, converts each JSON object into a TLV representation
/// and saves all converted objects into dest_file. All JSON keys are being saved into a keystore_file
/// @param pool a high-level memory pool. Will be a parent of all pools inside the function
/// All derived pools will be automatically destroyed after the funtion's work is done
/// @param source_file a JSON file. Source of data.
/// @param dest_file a destination file. Binary, stores TLV representations
/// @param keystore_file a file that stores all keys for TLV representations
/// @param max_line_length a maximum length of line that can be read
/// @return APR status. APR_SUCCESS if everything is ok
apr_status_t dump_json_log(apr_pool_t *pool, const char *source_file, const char *dest_file,
    const char *keystore_file, const size_t max_line_length);

/// @brief Reads a binary dump file and it's keystore to restore a json representation
/// @param pool a high-level memory pool. Will be a parent of all pools inside the function
/// All derived pools will be automatically destroyed after the funtion's work is done
/// @param dump_file a source file. Binary, stores TLV representations
/// @param keystore_file a file that stores all keys for TLV representations
/// @param dest_json_file a JSON file. The data will be restored there
/// @param max_line_length a maximum length of line that can be read
/// @return APR status. APR_SUCCESS if everything is ok
apr_status_t restore_json_from_dump(apr_pool_t *pool, const char *dump_file,
    const char *keystore_file, const char *dest_json_file, const size_t max_line_length);
