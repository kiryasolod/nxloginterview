cmake_minimum_required(VERSION 3.18)

project(NXLogInterview LANGUAGES C)
set(CMAKE_C_STANDARD 99)
list(APPEND src "parser/parser.c")
list(APPEND src "parser/utils.c")
list(APPEND src "io/io.c")
list(APPEND hdr "parser/parser.h")
list(APPEND hdr "parser/utils.h")
list(APPEND hdr "parser/types.h")
list(APPEND hdr "io/io.h")

add_executable(NXLogInterview ${hdr} ${src} main.c)
# Find JSON-C library
find_package(json-c CONFIG)
# Link JSON-C library
target_link_libraries(${PROJECT_NAME} PRIVATE json-c::json-c)
# Link APR library
target_link_libraries(${PROJECT_NAME} PRIVATE apr-1)

enable_testing()

set(TEST_NAME "test1")
add_executable(${TEST_NAME} tests/test.c ${hdr} ${src})
# Link JSON-C library
target_link_libraries(${TEST_NAME} PRIVATE json-c::json-c)
# Link APR library
target_link_libraries(${TEST_NAME} PRIVATE apr-1)

add_test(NAME Test1 COMMAND test1)
